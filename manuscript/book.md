---
subject: "Advanced Web Development with Scala"
keywords: [Scala, web, API, http4s, tapir, refined]
lang: "en"
book: true
classoption: [oneside]
footnotes-pretty: true
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "manuscript/images/titlepage.pdf"
toc-own-page: true
colorlinks: true
header-left: "Advanced Web Development with Scala"
header-right: "Jens Grassel"
footer-left: "\\rightmark"
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...

# Foreword

Thank you for your interest in this book. I hope you'll have a good time reading it and learn something from it.

## About this book

This book is intended as a follow-up (part 2) of "Pure functional HTTP APIs in Scala" It builds upon the usage of the tapir library to create web services entirely described by types - allowing the automatic derivation of server code, helper logic, documentation and even client code.

Code and book source can be found in the following repository:

[https://codeberg.org/jan0sch/Advanced_Web_Development_with_Scala](https://codeberg.org/jan0sch/Advanced_Web_Development_with_Scala)

## Copyleft Notice

This book uses the Creative Commons Attribution ShareAlike 4.0 International (CC BY-SA 4.0) license[^1]. The code snippets in this book are licensed under CC0[^2] which means you can use them without restriction. Excerpts from libraries maintain their license.

# Where are we coming from?

We left the last book having gained some insights upon the practical applicability of pure functional programming in the domain of web development, especially HTTP services. Several things we saw were:

1. better understandable code
2. better testable code
3. better performing code (Yes!)
4. better type safety using refined types
5. deriving boilerplate code from our statically typed models and logic
6. easier working with deeply nested structures using optics

But there is more to be gained and this book intends to guide you through another part of the journey.

# Our use case

Like in "Pure functional HTTP APIs in Scala"[^3] we will develop a small application with a limited use case. However this time we only use one implementation which allows us to dig into something a bit deeper. The following section will contain the specification.

## Application specification

To create a setting which will feel like a "real" web application we should add some more stuff besides a pure API. So what should our application provide in regards of functionality?

1. Some kind of initialisation after installation.
2. An administrator should be able to lock, unlock and delete accounts.
3. Users should be able to register through a web form.
4. Users should be able to login and manage some settings and data.
5. Users should be able to generate an API key.
6. Users should be able to delete their account.
7. The application should provide an API for uploading / managing data.
8. The uploaded data should be displayed on the website in some format.
9. Maybe the application has to perform queries on an external service.

As we can see this is slightly more than a "simple" HTTP API service. So let us break it down a bit more.

First our "definitions" are pretty vague like "some settings" or "upload data" - sadly it is not uncommon to get such requirements within a project. However one can always concentrate on other parts first and refined later on but we'll define our use case more detailed. We want a website which acts as a platform for collector's items. Because this area is really huge we narrow it down to books (possibly antiquarian ones). This allows us to refined our points (7) and (8) from above:

7. The application should provide an API for uploading and downloading lists of books and endpoints for managing (read, update, delete) single books.
8. The uploaded book lists should be displayed on the website as lists per user.

### Initialisation

There are several ways to add some basic initialisation to an application. You might think of hard coding some values or provide them via configuration files or parameters. However to make our application more user friendly we want to provide a small "wizard" to configure basic stuff. But what do we need and what makes sense?

We could try to configure just about everything in the initialisation phase but to keep things more simple we will just create an initial system account with special permissions (*read* an administrator). This should be enough to get us started and give you an idea. Please remember that for other things like settings you might want to write them into a configuration file and not into a database.

So here we go: The application must provide an initialisation under the URL path `/setup` after starting it. This process must allow the user to create an initial administrator account. If an administrator already exists in the configured database then an error message shall be displayed to the user.

### Administrative functionality

To keep things simple we concentrate upon essential administrative tasks here. An administrator must be able to display a list of all accounts, the details of an account and shall be allows to lock, unlock or delete an account. The deletion of an account should have a safety i.e. confirmation.

While talking about locking and unlocking we should realise that such a functionality must be reflected in our data model and could also be used to provide additional functionality like:

- locking a user account after a certain amount of failed logins
- providing an unlock link for a locked user
- possibly more?

But let's go on.

### User registration

A potential user must be able to register for an account using a form under the URL path `/register`. Again to keep things simple we only ask for an email address and a password. However we might want to disable (lock) the account after creation until the user confirms her email by using a link (unlock link?) emailed to her.

### User login

A user (but also an administrator) must be able to login via a form under the URL path `/login`. The users email address will be used as a login name.

### User functionality

After login the user should be able to enter some data which might be relevant for business purposes. For the sake of brevity we'll stick to a company name, address and a VAT number. Of course our users must be able to edit their data afterwards but also have a function to delete their account. Upon the deletion of the account all data related to the user must be removed.

Additionally it should be possible for the user to generate and regenerate an API key for our yet to be defined API.

## Data model

The application must be able to handle book data which we narrow down to some essentials because - as usual - there are lots of things to learn when diving into this field and possibly even more data we could think of to collect and store.

### Book

The following attributes should be supported by our "book" model and _must_ be present in the data which is uploaded via the API.

| Attribute | Type         | Required |
|-----------|--------------|:--------:|
| Author    | String       | Yes      |
| Title     | String       | Yes      |
| Book-ID   | String       | No       |
| Publisher | String       | Yes      |
| Year      | Int          | Yes      |
| Keywords  | List[String] | No       |

Several things are debatable here. For starters we do not have a unique identifier here, or have we? No, we haven't. Unique ids should be generated upon the upload for each entry and can then be used afterwards to update or identify single books.

Ok, next. What is this `Book-ID` attribute for, don't we have ISBN for this? Yes, we have ISBN to identify books but you won't find books older than from the 1960's with it. For older books there exists a variety of identifier formats and several may not have one. That is also why we do not make it a mandatory field.

However to avoid really messy data we require several other attributes which might inflict some work on the user side but will ease handling on the application side.

::: box
But how do I know what data to require (or allow) and which not?
:::

As usual there is no silver bullet which will solve this for every domain. But if there is something like a golden rule then it is this:

::: warning
Do not go down the road labelled "We must allow the user to upload _all_ kind of data into our application." - only grief and fragile software will you find!
:::

Try to define constraints that prevent you from writing thousands and thousands of lines of code to make sense out of a huge pile of bits. In reality this will always be a compromise between your data model and store and the available input and willingness of the users.

## Database

We'll use a classic relational database (RDBMS) to store our data. Which means we must define the needed tables and their relations within the database. The concrete pick is PostgreSQL[^5] but there are others available and you should chose depending on your needs and environment.

### The accounts table

All information about users will be stored within the `accounts` table. A unique ID for each user will be generated by the `java.util.UUID` class and therefore our `id` column will have the type `UUID` which is supported by PostgreSQL.

::: box
But why bother with `UUID` if there is `SERIAL` or `BIGSERIAL` available?
:::

Well, the obvious downside is that _we_ will have to take care of generating the user ID. However the convenience of having it automatically generated by the database has downsides too. For once you will have to pay attention to not pass the value in places where you shouldn't. Furthermore bad things may happen if you move data around (_read_ across databases).

We'll need more than an ID to have a useful dataset so let us add some more fields. The columns for email address and hashed(!) password go without saying. Additionally we want an "admin flag" which defaults to `FALSE` to be able to identify administrators. Regarding locking functionality we need columns for the number of failed login attempts, a date and time when the account was locked and last but not least an unlock token.

Regarding the constraints on the table the primary key on the ID column is obvious and also the email and unlock token must both be unique.

::: box
What are these `COMMENT ON TABLE` and `COMMENT ON COLUMN` directives?
:::

Well, as written in the last book: **Thou shall document your software!**

But regarding database schemas this is not as straightforward like directly in code. You can of course do comments directly in your SQL source code but what about someone who is looking directly at the database? They may not have access to the source code and maybe there is no documentation document or the database part is missing from that or ... you get the idea. So if we have the possibility to save our comments (_read_ our documentation) directly with the database schema then we should do so because it will be accessible to them.

::: info
It goes without saying that you have to keep it up to date like a regular documentation, however usually (_hopefully ;-)_) database schemas tend not to change too drastically.
:::

### The books table

**TODO**

## API specification

We want to expose an HTTP API to our users which is accessible via an API key.

::: box
But what if we do implement our endpoints _completely_ as an API?
:::

Probably you have heard this before and it _can_ make sense. In the long run it might give you an advantage regarding the de-coupling of user interface and back end code. However - like most of the time - it largely depends on the circumstances and available fellow humans. If there are already existing tools on the interface side (e.g. think of some fancy JavaScript framework here) then it can be a good idea to go "full API" right from the start. In practice this means that your endpoints will usually only handle JSON (input and output). The front end people can then unleash their tooling and create the user interface.

On the other hand it is not the worst choice to _keep things simple_. In fact there is quite a challenge in not over-engineering things. Making your endpoints receive and return HTML is a good starting point and quite often more than sufficient. You can always add a JSON layer later on to open up more possibilities.

::: info
Also note that sadly it is common that JavaScript only sites break accessibility in a multitude of ways.
:::

For more information regarding accessibility reading the Wikipedia article[^4] is a good starting point which contains further links.

### Base URL

The base URL path for the API will be `/api` and all other endpoints will be put behind it e.g. `/api/foo/bar`.

### Authentication

As mentioned we want to protect our API with an API key. The key has to be a non empty string which must be passed in a header field of the HTTP request. The header field has to be named `API-Key`.
The actual key will be generated per user.

### Upload books

The endpoint for uploading lists of books will be available at `/books` via a HTTP `POST` request which must be of the type `multipart/form-data`. The form data must include a file (field named `books`) which holds the list of books that shall be uploaded. An additional form field named `uid` must include the ID of the user matching the provided API key.

Book list files must be in TSV format obeying the following rules:

1. file encoding must be UTF-8
2. column separator must be a tab character (`\t`)
3. first line must include the column names (_read_ headers)
4. columns are defined as follows:
    1. `uuid`
    2. `id`
    3. `author`
    4. `title`
    5. `year`
    6. `publisher`
    7. `keywords`
5. columns `uuid`, `id` and `keywords` may be empty
6. column `keywords` contains a comma (`,`) separated list of words

If the column `uuid` is not empty then a probably existing book entry with that UUID will be updated (_read_ overwritten) with the data from the file.

### Download books

Via the endpoint `/books` using a HTTP `GET` request a TSV file containing all of the books can be downloaded. The file format is defined as follows:

1. file encoding must be UTF-8
2. column separator must be a tab character (`\t`)
3. first line must include the column names (_read_ headers)
4. columns are defined as follows:
    1. `uuid`
    2. `id`
    3. `author`
    4. `title`
    5. `year`
    6. `publisher`
    7. `keywords`
5. columns `id` and `keywords` may be empty
6. column `keywords` contains a comma (`,`) separated list of words

The download file format equals the upload format except that the `uuid` column will never be empty.

### Reading a single book

The endpoint `/book/{uuid}` using a HTTP `GET` request will return the data for a single book with the given UUID in JSON format or a 404 error.

### Deleting a single book

Via the endpoint `/book/{uuid}` using a HTTP `DELETE` request the matching book will be deleted if it belongs to the user. The server will respond either with a `XXX - no data` or with a `404 - Not Found` response. **TODO**

### Updating a single book

To update a book the endpoint `/book/{uuid}` has to be used with a HTTP `PUT` request. **TODO**

# Getting things done

**TODO**

# Epilogue

**TODO**

# Thanks

First and foremost I want to thank my wonderful wife and family yet again. Without their constant support none of this would have been possible.

Further thanks go to the people who have read "Pure functional HTTP APIs in Scala" and gave feedback and also encouraged me to write a follow up book.

Additionally I want to thank the fellow humans from the Scala community which were always there to lend support by answering questions or giving suggestions.

::: info
Last but not least a big **"Thank you!"** goes to you dear reader!
:::

[^1]: https://creativecommons.org/licenses/by-sa/4.0/legalcode

[^2]: https://wiki.creativecommons.org/wiki/CC0

[^3]: https://leanpub.com/pfhais

[^4]: https://en.wikipedia.org/wiki/Web_accessibility

[^5]: https://www.postgresql.org

