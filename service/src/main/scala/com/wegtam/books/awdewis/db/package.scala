/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis

import eu.timepit.refined._
import eu.timepit.refined.api._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.collection._
import eu.timepit.refined.string._

package object db {
  type JDBCDriverName =
    String Refined MatchesRegex[W.`"^\\\\w+\\\\.[\\\\w\\\\d\\\\.]+[\\\\w\\\\d]+$"`.T]
  object JDBCDriverName extends RefinedTypeOps[JDBCDriverName, String] with CatsRefinedTypeOpsSyntax

  type JDBCUrl = String Refined MatchesRegex[W.`"^jdbc:[a-zA-z0-9]+:.*"`.T]
  object JDBCUrl extends RefinedTypeOps[JDBCUrl, String] with CatsRefinedTypeOpsSyntax

  type JDBCUsername = String Refined NonEmpty
  object JDBCUsername extends RefinedTypeOps[JDBCUsername, String] with CatsRefinedTypeOpsSyntax

  type JDBCPassword = Array[Byte] Refined NonEmpty
  object JDBCPassword
      extends RefinedTypeOps[JDBCPassword, Array[Byte]]
      with CatsRefinedTypeOpsSyntax
}
