/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis.db

import java.nio.charset.StandardCharsets

import cats.effect.IO
import eu.timepit.refined.auto._
import org.flywaydb.core.Flyway

final class FlywayDatabaseMigrator {

  /**
    * Apply pending migrations to the database.
    *
    * @param url  A JDBC database connection url.
    * @param user The login name for the connection.
    * @param pass The password for the connection.
    * @return The number of applied migrations.
    */
  def migrate(url: JDBCUrl, user: JDBCUsername, pass: JDBCPassword): IO[Int] =
    IO {
      val flyway: Flyway =
        Flyway.configure().dataSource(url, user, new String(pass, StandardCharsets.UTF_8)).load()
      flyway.migrate()
    }

}
