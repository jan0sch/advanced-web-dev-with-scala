/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis

import java.util.concurrent.Executors

import cats.effect._
import cats.implicits._
import com.typesafe.config._
import com.wegtam.books.awdewis.api._
import com.wegtam.books.awdewis.config._
import com.wegtam.books.awdewis.db.FlywayDatabaseMigrator
import eu.timepit.refined.auto._
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze._
import pureconfig._
import sttp.tapir.docs.openapi._
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.swagger.http4s.SwaggerHttp4s

import scala.concurrent.ExecutionContext

object Server extends IOApp.WithContext {
  val ec: ExecutionContext = ExecutionContext.global

  override protected def executionContextResource: Resource[SyncIO, ExecutionContext] =
    Resource.liftF(SyncIO(ec))

  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  override def run(args: List[String]): IO[ExitCode] = {
    val migrator    = new FlywayDatabaseMigrator
    val specialPool = Executors.newFixedThreadPool(2)
    val _           = ExecutionContext.fromExecutor(specialPool) // FIXME

    val program = for {
      config <- IO(ConfigFactory.load(getClass().getClassLoader()))
      dbConfig <- IO(
        ConfigSource.fromConfig(config).at(DatabaseConfig.CONFIG_KEY).loadOrThrow[DatabaseConfig]
      )
      serviceConfig <- IO(
        ConfigSource.fromConfig(config).at(ServiceConfig.CONFIG_KEY).loadOrThrow[ServiceConfig]
      )
      _ <- migrator.migrate(dbConfig.url, dbConfig.user, dbConfig.pass)
      helloWorldRoutes = new HelloWorld[IO]
      docs             = List(HelloWorld.greetings).toOpenAPI("Service Name", "1.0.0")
      swagger          = new SwaggerHttp4s(docs.toYaml)
      routes           = helloWorldRoutes.routes <+> swagger.routes[IO]
      httpApp          = Router("/" -> routes).orNotFound
      server = BlazeServerBuilder[IO](ec)
        .bindHttp(serviceConfig.port, serviceConfig.ip)
        .withHttpApp(httpApp)
      fiber = server.serve.compile.drain.as(ExitCode.Success)
      _ <- IO.delay(specialPool.shutdown()) // Stop our created thread pool.
    } yield fiber
    program.attempt.unsafeRunSync() match {
      case Left(e) =>
        IO {
          e.printStackTrace()
          ExitCode.Error
        }
      case Right(s) => s
    }
  }

}
