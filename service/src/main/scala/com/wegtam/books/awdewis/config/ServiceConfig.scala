/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis.config

import eu.timepit.refined.auto._
import eu.timepit.refined.pureconfig._
import eu.timepit.refined.types.net.PortNumber
import pureconfig._
import pureconfig.generic.semiauto._

/**
  * The service configuration.
  *
  * @param ip   The ip address the service will listen on.
  * @param port The port number the service will listen on.
  */
final case class ServiceConfig(ip: IP, port: PortNumber)

object ServiceConfig {
  // The default configuration key to lookup the service configuration.
  final val CONFIG_KEY: ConfigKey    = "service"
  final val DEFAULT_IP: IP           = "127.0.0.1"
  final val DEFAULT_PORT: PortNumber = 8080

  implicit val serviceConfigReader: ConfigReader[ServiceConfig] = deriveReader[ServiceConfig]

}
