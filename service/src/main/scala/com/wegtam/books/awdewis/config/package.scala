/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis

import eu.timepit.refined.api._
import eu.timepit.refined.cats._
import eu.timepit.refined.collection._
import eu.timepit.refined.string._

package object config {

  type ConfigKey = String Refined NonEmpty
  object ConfigKey extends RefinedTypeOps[ConfigKey, String] with CatsRefinedTypeOpsSyntax

  type IP = String Refined IPv4
  object IP extends RefinedTypeOps[IP, String] with CatsRefinedTypeOpsSyntax

}
