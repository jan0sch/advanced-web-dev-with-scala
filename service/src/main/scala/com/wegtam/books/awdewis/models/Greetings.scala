/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis.models

import eu.timepit.refined.auto._
import io.circe._
import io.circe.generic.semiauto._
import io.circe.refined._

/**
  * A simple model for our hello world greetings.
  *
  * @param title    A generic title.
  * @param headings Some header which might be presented prominently to the user.
  * @param message  A message for the user.
  */
final case class Greetings(title: GreetingTitle, headings: GreetingHeader, message: GreetingMessage)

object Greetings {

  implicit val decoder: Decoder[Greetings] = deriveDecoder[Greetings]
  implicit val encoder: Encoder[Greetings] = deriveEncoder[Greetings]

}
