/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis.config

import eu.timepit.refined.auto._
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import com.typesafe.config.ConfigFactory
import pureconfig._

class DatabaseConfigTest extends AnyWordSpec with Matchers {

  "DatabaseConfig" when {
    "loading the default application.conf" must {
      "read the configuration correctly" in {
        val cfg = ConfigFactory.load(getClass().getClassLoader())
        ConfigSource.fromConfig(cfg).at(DatabaseConfig.CONFIG_KEY).load[DatabaseConfig] match {
          case Left(e)  => fail(e.toList.mkString(", "))
          case Right(_) => succeed
        }
      }
    }
  }

}
