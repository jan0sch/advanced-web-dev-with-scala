/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis.api

import cats.effect._
import com.wegtam.books.awdewis.BaseSpec
import com.wegtam.books.awdewis.models._
import eu.timepit.refined.auto._
import eu.timepit.refined.types.string.NonEmptyString
import org.http4s._
import org.http4s.circe._
import org.http4s.implicits._
import org.http4s.server.Router

class HelloWorldTest extends BaseSpec {
  implicit val contextShift: ContextShift[IO] = IO.contextShift(executionContext)
  implicit val timer: Timer[IO]               = IO.timer(executionContext)
  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  implicit def decodeGreetings: EntityDecoder[IO, Greetings] = jsonOf

  "HelloWorld" when {
    "parameter 'name' is missing" must {
      val expectedStatusCode = Status.BadRequest

      s"return $expectedStatusCode" in {
        Uri.fromString("/hello") match {
          case Left(_) =>
            fail("Could not generate valid URI!")
          case Right(u) =>
            def service: HttpRoutes[IO] = Router("/" -> new HelloWorld[IO].routes)
            val request = Request[IO](
              method = Method.GET,
              uri = u
            )
            val response = service.orNotFound.run(request)
            for {
              result <- response.unsafeToFuture()
              body   <- result.as[String].unsafeToFuture()
            } yield {
              result.status must be(expectedStatusCode)
              body must be("Invalid value for: query parameter name")
            }
        }
      }
    }

    "parameter 'name' is set" when {
      "parameter value is invalid" must {
        val expectedStatusCode = Status.BadRequest

        s"return $expectedStatusCode" in {
          Uri.fromString("/hello?name=") match {
            case Left(_) =>
              fail("Could not generate valid URI!")
            case Right(u) =>
              def service: HttpRoutes[IO] = Router("/" -> new HelloWorld[IO].routes)
              val request = Request[IO](
                method = Method.GET,
                uri = u
              )
              val response = service.orNotFound.run(request)
              for {
                result <- response.unsafeToFuture()
                body   <- result.as[String].unsafeToFuture()
              } yield {
                result.status must be(expectedStatusCode)
                body must be(
                  "Invalid value for: query parameter name (expected value to have length greater than or equal to 1, but was )"
                )
              }
          }
        }
      }

      "parameter value is valid" must {
        val expectedStatusCode = Status.Ok

        s"return $expectedStatusCode" in {
          val name: NonEmptyString = "Captain Kirk"
          val expectedGreetings = Greetings(
            title = "Hello Captain Kirk!",
            headings = "Hello Captain Kirk, live long and prosper!",
            message = "This is a fancy message directly from http4s! :-)"
          )
          Uri.fromString(Uri.encode(s"/hello?name=$name")) match {
            case Left(e) =>
              println(e)
              fail("Could not generate valid URI!")
            case Right(u) =>
              def service: HttpRoutes[IO] = Router("/" -> new HelloWorld[IO].routes)
              val request = Request[IO](
                method = Method.GET,
                uri = u
              )
              val response = service.orNotFound.run(request)
              for {
                result <- response.unsafeToFuture()
                body   <- result.as[Greetings].unsafeToFuture()
              } yield {
                result.status must be(expectedStatusCode)
                body must be(expectedGreetings)
              }
          }
        }
      }
    }
  }

}
