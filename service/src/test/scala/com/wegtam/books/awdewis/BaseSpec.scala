/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis

import java.util.concurrent.Executors

import com.typesafe.config._
import com.wegtam.books.awdewis.config._
import eu.timepit.refined.auto._
import pureconfig._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach }
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AsyncWordSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import cats.effect.Blocker

/**
  * A base class for our tests.
  */
abstract class BaseSpec
    extends AsyncWordSpec
    with Matchers
    with ScalaCheckPropertyChecks
    with BeforeAndAfterAll
    with BeforeAndAfterEach {

  protected val config        = ConfigFactory.load()
  protected val dbConfig      = ConfigSource.fromConfig(config).at("database").load[DatabaseConfig]
  protected val serviceConfig = ConfigSource.fromConfig(config).at("service").load[ServiceConfig]

}

object BaseSpec {
  val blockingPool = Executors.newFixedThreadPool(2)
  val blocker      = Blocker.liftExecutorService(blockingPool)
}
