# Advanced Web Development with Scala #

This repository contains the source code for the book "Advanced Web 
Development with Scala" which is available on Leanpub: 
https://leanpub.com/advanced-web-development-with-scala

## Structure ##

The folder `manuscript` contains the book source code and other resources 
like images.

This book uses `pandoc` and the `pandoc-latex-environment`. The latter can
be installed using the following command:

```
% pip-3.7 install --user --upgrade pandoc-latex-environment
```

### Build Tooling ###

All project modules can be compiled, run and tested via [sbt](https://www.scala-sbt.org/).
Just open a terminal in the `service` folder and start the `sbt` shell.

If you are using the IntelliJ IDEA development environment then you will 
need to install the Scala plugin for it. Afterwards you should be able to 
create IDEA projects by using the "Import Project" feature and point it to 
the `service` folder.

## Copyleft Notice ##

This book uses the Creative Commons Attribution ShareAlike 4.0 International 
(CC BY-SA 4.0) license. The code snippets in this book are licensed under 
CC0 which means you can use them without restriction. 
Excerpts from libraries maintain their license.

