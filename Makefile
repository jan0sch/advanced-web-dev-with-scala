# If using fontawesome we have to switch to another pdf engine:
# --pdf-engine=lualatex
PANDOC=pandoc --from markdown --filter pandoc-latex-environment \
       --listings --standalone --toc \
       --top-level-division=chapter

SOURCE=manuscript/book.md

clean:
	rm -rf dist

dist:
	mkdir dist

html: dist
	${PANDOC} --metadata title="Advanced Web Development with Scala" \
	--metadata author="Jens Grassel" \
	--to html5 ${SOURCE} -o dist/book.html

pdf: dist
	${PANDOC} --template manuscript/templates/eisvogel.latex \
	${SOURCE} -o dist/book.pdf

epub: dist
	${PANDOC} --metadata title="Advanced Web Development with Scala"\
	--metadata author="Jens Grassel" \
	--to epub3 ${SOURCE} -o dist/book.epub

all: epub html pdf

.MAIN: pdf

